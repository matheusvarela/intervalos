Classes de equivalencia

| Variáveis de Entrada |        Condições	  |                        Classes Válidas                          |           Classes Inválidas             | Saída Esperada  |
|----------------------|--------------------------|-----------------------------------------------------------------|--------------------------------------------------------------|-----------------|
|         Lista          | Tamanho da lista tem que ser maior que zero  |	                        lista maior que zero                             |                            lista igual a vazio                 |      Uma lista de intervalos |
|                      |                    Elementos da lista tem que ser numeros inteiros      | lista = [1,2,3,4,2,3,4]   | lista = [1, 2, a, b1 ]  |     |   |

Tabela para condições válidas

| Variável de entrada  |
|----------------------|
|       lista = [1,2,3,4,10,11,12]    |
|       lista = [10,11,12,20,21,23]     |


Tabela para condições inválidas

|  Variável de entrada |
|----------------------|
|       lista = [a,b,c,d,e]     |
|       lista = [1,2,3, a, b, 4, 5]     |
|       lista = []        |
