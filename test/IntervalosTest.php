<?php
// Agradeço a DEUS pelo o dom do conhecimento

use PHPUnit\Framework\TestCase;

final class IntervalosTest extends TestCase
{

  public function testIntervalosValido()
  {
    $this->assertEquals(
	[
		[100, 105],
		[110, 111],
		[113, 115],
		[150]
	],
        Intervalos::verificarIntervalos([100, 101, 102, 103, 104, 105, 110, 111, 113, 114, 115, 150])
      );
  }

  public function testIntervalosValido1()
  {
    $this->assertEquals(
	[
		[100, 102],
		[105],
		[110, 112]
	],
        Intervalos::verificarIntervalos([100, 101, 102, 105, 110, 111, 112])
      );
  }

  public function testIntervalosInvalido()
  {
    $this->assertFalse(
      Intervalos::verificarIntervalos([1,'a1', 2,'b','x2c'])
    );
  }

  public function testIntervalosInvalido1()
  {
    $this->assertTrue(
      Intervalos::verificarIntervalos([])
    );
  }

}


 ?>
